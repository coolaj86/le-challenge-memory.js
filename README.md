| [letsencrypt](https://git.rootprojects.org/root/greenlock.js) (library)
| [letsencrypt-cli](https://git.rootprojects.org/root/greenlock-cli.js)
| [letsencrypt-express](https://git.rootprojects.org/root/greenlock-express.js)
| [letsencrypt-koa](https://git.rootprojects.org/root/greenlock-koa.js)
| [letsencrypt-hapi](https://git.rootprojects.org/root/greenlock-hapi.js)
|

# le-challenge-memory

An in-memory strategy for node-letsencrypt for setting, retrieving,
and clearing ACME challenges issued by the ACME server

* Safe to use on ephemeral services (i.e. AWS)

## Install

```bash
npm install --save le-challenge-standalone@2.x
```

## Usage

```bash
var leChallenge = require('le-challenge-standalone').create({
, debug: false
});

var LE = require('greenlock');

LE.create({
  server: LE.stagingServerUrl
, challenge: leChallenge
});
```

NOTE: If you request a certificate with 6 domains listed,
it will require 6 individual challenges.

## Exposed Methods

For ACME Challenge:

* `set(opts, domain, key, val, done)`
* `get(defaults, domain, key, done)`
* `remove(defaults, domain, key, done)`

# Legal &amp; Rules of the Road

Greenlock&trade; and Bluecrypt&trade; are [trademarks](https://rootprojects.org/legal/#trademark) of AJ ONeal

The rule of thumb is "attribute, but don't confuse". For example:

> Built with [Greenlock](https://git.rootprojects.org/root/greenlock.js) (a [Root](https://rootprojects.org) project).

Please [contact us](mailto:aj@therootcompany.com) if you have any questions in regards to our trademark,
attribution, and/or visible source policies. We want to build great software and a great community.

[Greenlock&trade;](https://git.rootprojects.org/root/greenlock.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)
